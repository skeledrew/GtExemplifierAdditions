# GtExemplifierAdditions
A collection of additions to the [GToolkit](https://gtoolkit.com/) exemplifier tool.

## Components
- Example parametrization.

## Installation
- This package should be installable via the Gt4Git tool.
- It can also be installed by executing in a Playground:
```
Metacello new
    baseline: 'GtExemplifierAdditions';
    repository: 'gitlab://skeledrew/GtExemplifierAdditions';
    load.
```

## Usage
- Create a class containing examples.
- The primary parametrization provider is `TGeaParametrizeSupport`. See demo and comments in the classes for further information.

## License
- AGPL. See LICENSE.
