"
I am a demo example class with a regular example, and parametrized examples with input collection inside and outside.
"
Class {
	#name : #GeaParametrizedExampleExample,
	#superclass : #Object,
	#traits : 'TGeaParametrizeSupport',
	#classTraits : 'TGeaParametrizeSupport classTrait',
	#instVars : [
		'mathExampleParams',
		'stringExampleParams'
	],
	#category : #'GtExemplifierAdditions-Parametrizing-Examples'
}

{ #category : #accessing }
GeaParametrizedExampleExample >> add: theOperands [
    ^ theOperands first + theOperands second
]

{ #category : #accessing }
GeaParametrizedExampleExample >> div: theOperands [
    ^ theOperands first / theOperands second
]

{ #category : #accessing }
GeaParametrizedExampleExample >> doBasicMath: anOperatorSymbol on: anOperandInteger and: anotherOperandInteger andExpect: aResultInteger [
    "Parametrized example with internal parameter collection."
    <gtExample>
    <parametrizeUsing: #mathExampleParams>
    | result |
	self getParamsFor: #mathExampleParams from: [
	    GeaParameterSets new 
	        paramNames:#(anOperatorSymbol anOperandInteger anotherOperandInteger aResultInteger);
	        paramValidators: {Symbol. Integer. Integer. Integer}; "optional"
	        add: #(add: 3 5 8);
	        add: #(sub: 3 2 0); "fail example"
	        add: #(mul: 7 5 35);
	        add: #(div: 1 0 0). "error example"
	].
    result := self perform: anOperatorSymbol withArguments: {{anOperandInteger. anotherOperandInteger}}.
    self assert: result = aResultInteger.
    ^ result
]

{ #category : #accessing }
GeaParametrizedExampleExample >> flipFalseToTrue [
    "Regular example."
    <gtExample>
    | result |
	result := false not.
    self assert: result = true.
    ^ result.
]

{ #category : #accessing }
GeaParametrizedExampleExample >> getParamsFor: aSymbol from: aBlock [
    "Get parameters for the example."
    | params setter addedSetter |
    isCollectingParametersFlag ifNil: [^ false].
    setter := (aSymbol asString, ':') asSymbol.
    (self respondsTo: setter) ifFalse: [
        self class compile: 
            setter asString, ' anObject ', aSymbol asString, ' := anObject'.
        addedSetter := true.
    ].
	[
	    self perform: setter withArguments: {aBlock value}. 
	    GeaParameterCollectionCompleteSignal new signal
	]
        on: GeaParameterCollectionCompleteSignal 
        do: [
            addedSetter ifTrue: ["TODO: delete setter method" nil].
            ^ GeaParameterCollectionCompleteSignal signal. "reraise to break out, if collecting within an example"
        ].
]

{ #category : #'as yet unclassified' }
GeaParametrizedExampleExample >> mathExampleParams: anObject [ mathExampleParams := anObject
]

{ #category : #accessing }
GeaParametrizedExampleExample >> mul: theOperands [
    ^ theOperands first * theOperands second
]

{ #category : #accessing }
GeaParametrizedExampleExample >> sliceString: aString with: aStartInteger and: anEndInteger andExpect: aResultString [
    "Parametrized example with external parameter collection."
    <gtExample>
    <parametrizeUsing: #stringExampleParams>
    | result |
	result := (aString allButFirst: aStartInteger) allButLast: anEndInteger.
	self assert: result = aResultString.
	^ result.
]

{ #category : #accessing }
GeaParametrizedExampleExample >> stringExampleParams [
    ^ GeaParameterSets new
        paramNames: #(aString aStartInteger anEndInteger aResultString);
        + #('hello' 1 0 'ello');
        + #('world' 1 1 'orl');
        + #('grade' 0 1 'grad').
]

{ #category : #accessing }
GeaParametrizedExampleExample >> sub: theOperands [
    ^ theOperands first - theOperands second
]
