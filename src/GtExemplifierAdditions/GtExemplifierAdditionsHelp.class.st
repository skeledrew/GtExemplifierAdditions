"
# GtExemplifierAdditions
A collection of additions to the [GToolkit](https://gtoolkit.com/) exemplifier tool.

## Components
- Example parametrization.
- Trace debugging utility.
- Language-agnostic debugger interface.

## Installation
- This package should be installable via the Gt4Git tool.
- It can also be installed by executing in a Playground:
```
Metacello new
    baseline: 'GtExemplifierAdditions';
    repository: 'gitlab://skeledrew/GtExemplifierAdditions';
    load.
```

## Usage
- Create a class containing examples.
- The primary parametrization provider is `TGeaParametrizeSupport`. See demo and comments in the classes for further information.

## License
- AGPLv3+. See LICENSE.
"
Class {
	#name : #GtExemplifierAdditionsHelp,
	#superclass : #Object,
	#category : #'GtExemplifierAdditions-Documentation'
}
