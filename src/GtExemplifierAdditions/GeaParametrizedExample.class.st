"
I am a wrapper for GtExample and I facilitate the collection and spcification of example parameter sets.

***See TODO on refactoring in `#at:`***
"
Class {
	#name : #GeaParametrizedExample,
	#superclass : #GtExample,
	#instVars : [
		'paramSets',
		'paramIndex',
		'example',
		'paramSet',
		'arguments'
	],
	#category : #'GtExemplifierAdditions-Parametrizing-Wrappers'
}

{ #category : #accessing }
GeaParametrizedExample >> arguments [
    ^ arguments.
]

{ #category : #accessing }
GeaParametrizedExample >> arguments: aCollection [
    arguments := aCollection.
]

{ #category : #accessing }
GeaParametrizedExample >> at: idx [
    "Create a new example with a single specified parameter set.
    
    TODO: this class needs refactoring... it's now serving both as a carrier of all parameter sets AND carrier of a single set to the evaluation point.
    "
    | cm |
	cm := (self example provider class >> self example selector).
	^ GeaParametrizedExample new
	initializeFromMethod: cm withProviderClass: cm methodClass usingFactory: GtParametrizedExampleFactory new;
	arguments: (paramSets at: idx).
]

{ #category : #accessing }
GeaParametrizedExample >> collectParamSets: aCompiledMethod [
    "Collect the sets of parameters for the example."
    | pragmas paramsVariable dummyArgs cls aParameterizedExampleObject params |
	pragmas := aCompiledMethod pragmas.
    paramsVariable := (pragmas select: [:p | p selector = #parametrizeUsing:]) first arguments first.
    cls := aCompiledMethod methodClass.
	aParameterizedExampleObject := cls new.
	(aParameterizedExampleObject respondsTo: paramsVariable) ifTrue: [
        params := aParameterizedExampleObject perform: paramsVariable.
        (params isKindOf: OrderedCollection) | (params isMemberOf: GeaParameterSets)
            ifTrue: [^ params]
            ifFalse: [self error: 
                'Getter `#', paramsVariable, '` exists, but returns invalid data. Please remove it if the example parameters are collected inside the example, or ensure it returns valid data if collected outside'
            ].
    ].
	dummyArgs := OrderedCollection new.
	selector := aCompiledMethod selector.
	1 to: (selector numArgs) do: [:idx | dummyArgs add: 'nil'].
	aParameterizedExampleObject isCollectingParametersFlag: true.
	[aParameterizedExampleObject perform: selector withArguments: dummyArgs asArray] 
	    on: GeaParameterCollectionCompleteSignal 
	    do: [true].
	aParameterizedExampleObject isCollectingParametersFlag: nil.
    cls compile: 
        paramsVariable, ' ^ ', paramsVariable.
	params := aParameterizedExampleObject perform: paramsVariable.
    cls removeSelector: paramsVariable.
    ^ params.
]

{ #category : #accessing }
GeaParametrizedExample >> evaluationContext [
	<return: #GtExampleEvaluationContext>
	^ GtExampleEvaluationContext new 
			provider: self provider;
			arguments: self arguments.
]

{ #category : #accessing }
GeaParametrizedExample >> evaluator [
	^ GeaExampleEvaluator new 
		example: self;
		yourself
]

{ #category : #accessing }
GeaParametrizedExample >> example [
    ^ example
]

{ #category : #accessing }
GeaParametrizedExample >> example: aGtExampleObject [
    "Wrap aGtExampleObject and collect the param sets."
    | pragmas |
	example := aGtExampleObject.
	paramSets := self collectParamSets: aGtExampleObject providerClass >> aGtExampleObject selector.
	paramIndex := 0.
]

{ #category : #accessing }
GeaParametrizedExample >> incrementIndex [
    paramIndex := paramIndex + 1.
]

{ #category : #accessing }
GeaParametrizedExample >> paramSets [
    ^ paramSets.
]
