"
I hold the data specific to a single debug step: the time, node, method, variables, etc. I also provide views on that data.
"
Class {
	#name : #GeaTracePoint,
	#superclass : #Object,
	#instVars : [
		'editor',
		'node',
		'snapTime',
		'startTime',
		'variables',
		'loop',
		'level',
		'evalable'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaTracePoint >> editor: anEditor [
    editor := anEditor.
]

{ #category : #accessing }
GeaTracePoint >> evalable [
    ^ evalable.
]

{ #category : #accessing }
GeaTracePoint >> evalable: aGeaAbstractDebugEvalable [
    evalable := aGeaAbstractDebugEvalable.
]

{ #category : #accessing }
GeaTracePoint >> gtDetailsFor: aView [
    <gtView>
    | srcEd |
	^ aView explicit
        title: 'Details';
        priority: 10;
        stencil: [
            | aContainer |
            "aContainer := BlElement new.
            aContainer addChild: ((self gtSourceFor: aView) asElement);
                addChild: ((self gtVariablesFor: aView) asElement)."
            aContainer := BlElement new 
                layout: BlLinearLayout vertical;
                constraintsDo: [:c | c vertical matchParent. c horizontal matchParent].
            srcEd := (self gtSourceFor: aView) asElement.
            srcEd constraintsDo: [:c | c vertical matchParent. c horizontal matchParent].
            "srcEd addChild: (BrResizer new
                beBottom;
                target: srcEd;
                aptitude: BrGlamorousResizerAptitude;
                elevation: (BlRelativeElevation elevation: 1000);
                constraintsDo: [ :c | c ignoreByLayout ])."
            aContainer addChild: srcEd.
            aContainer addChild: ((self gtVariablesFor: aView) asElement).
        ]
]

{ #category : #accessing }
GeaTracePoint >> gtOverviewFor: aView [
    <gtView>
    ^ aView columnedList
        title: 'Overview';
        priority: 11;
        items: {
            {'Time offset' . snapTime - startTime}.
            {'Node' . node innerNode}.
            {'Evalable' . evalable evalable}.
            {'Total variables' . variables size}.
            {'Depth' . level}.
            {'Increment' . loop}.
        };
        column: 'Key' text: [:var | var first];
        column: 'Value' text: [:var | var second].
]

{ #category : #accessing }
GeaTracePoint >> gtSourceFor: aView [
	<gtView>
	"node methodNode ifNil: [ ^ aView empty ]."
	^ aView explicit
		title: 'Source';
		priority: 12;
		"actionButtonIcon: BrGlamorousVectorIcons browse
			tooltip: 'Browse'
			action: [ node methodNode compiledMethod gtBrowse ];"
		stencil: [ | ed text |
			text := node source asRopedText.
			node stop > 1 ifTrue: [text
				attributes:
					{(BlTextUnderlineAttribute new
						color: BrGlamorousColors textHighlightColor;
						thickness: 3;
						beNotOverwritableByStyler)}
				from: node start
				to: node stop].
			ed := BrEditor new
				text: text;
				aptitude: BrGlamorousCodeEditorAptitude new;
				"styler:
					(BrRBTextStyler new
						classOrMetaClass: node methodNode compiledMethod methodClass);"
		scrollToPosition: ( node source lineNumberCorrespondingToIndex: node start);
			    yourself. ]
]

{ #category : #accessing }
GeaTracePoint >> gtVariablesFor: aView [
    <gtView>
    ^ aView columnedList
        title: 'Variables';
        priority: 13;
        items: variables;
        column: 'Icon'
			icon: [ :var | 
				[ var second gtInspectorIcon
					ifNil: [ var second class systemIcon ] ]
					on: Error
					do: [ :error | self iconNamed: #smallWarningIcon ] ]
			width: 36;
		column: 'Variable' text: [:var | var first];
        column: 'Value' text: [ :var | 
			var second class = GeaTracePointException
			    ifFalse: [var second gtDisplayString asRopedText]
			    ifTrue: [var second exception errorMessage asRopedText foreground: Color red].
			"[ var second gtDisplayString asRopedText ]
			on: Error , Halt
			do: [:ex | ex errorMessage asRopedText foreground: Color red ]" ];
		send: #second.
]

{ #category : #accessing }
GeaTracePoint >> isValid [
    ^ node isValid.
]

{ #category : #accessing }
GeaTracePoint >> level [
    ^ level.
]

{ #category : #accessing }
GeaTracePoint >> level: anInteger [
    level := anInteger.
]

{ #category : #accessing }
GeaTracePoint >> loop [
    ^ loop.
]

{ #category : #accessing }
GeaTracePoint >> loop: anInteger [
    loop := anInteger.
]

{ #category : #accessing }
GeaTracePoint >> node [
    ^ node.
]

{ #category : #accessing }
GeaTracePoint >> node: aNode [
    node := aNode.
]

{ #category : #accessing }
GeaTracePoint >> offset [
    ^ snapTime - startTime.
]

{ #category : #accessing }
GeaTracePoint >> printOn: aStream [
    | nodeSrc location offset |
	nodeSrc := node nodeSrc.
	location := node printLocation.
	offset := (snapTime - startTime) asString.
    super printOn: aStream.
    aStream
        nextPutAll: '(';
        nextPutAll: loop asString;
        nextPut: $-;
        nextPutAll: level asString;
        nextPutAll: ': `';
        nextPutAll: nodeSrc;
        nextPutAll: '` in `';
        nextPutAll: location;
        nextPutAll: '` at ';
        nextPutAll: offset;
        nextPut: $).
]

{ #category : #accessing }
GeaTracePoint >> snapTime: aTime [
    snapTime := aTime.
]

{ #category : #accessing }
GeaTracePoint >> startTime: aTime [
    startTime := aTime.
]

{ #category : #accessing }
GeaTracePoint >> times [
    ^ {startTime . snapTime}.
]

{ #category : #accessing }
GeaTracePoint >> variables [
    ^ variables.
]

{ #category : #accessing }
GeaTracePoint >> variables: aVariablesCollection [
    variables := aVariablesCollection.
]
