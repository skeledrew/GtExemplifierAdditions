"
I am a collector of parameter sets for parametrized examples.

Public API:
- I provide the `#paramNames:` setter which takes an array of the parameter names as symbols which will be passed into the associated example. This setter must be used first on me.
- I provide the `#paramValidators:` method, which takes an array of classes or blocks which are used to validate the corresponding parameters. This is optional, but if used, must be second.
- I provide the `#add:` method, which takes an array of parameters (must correspond to the names), optionally validates them (if validators are given), and adds to my collection of parameter sets.
- I provide the `#+` method, which delegates to `#add:` and allows for easier addition.
"
Class {
	#name : #GeaParameterSets,
	#superclass : #Object,
	#instVars : [
		'paramNames',
		'params',
		'currentParamPointer',
		'paramValidators',
		'assertMessageMaker'
	],
	#category : #'GtExemplifierAdditions-Parametrizing-Core'
}

{ #category : #accessing }
GeaParameterSets >> + aCollection [
    "Allow aCollection to be added, and addition chains to be created, more easily."
    self add: aCollection.
    ^ self.
]

{ #category : #accessing }
GeaParameterSets >> add: aCollection [
    "Perform validation and add the parameter set."
    | param validator coll message |
    (paramNames size = aCollection size or: [paramNames size = (aCollection size + 1)])
	    "ifTrue: [assertMessageMaker ifNotNil: [self error: 'Since an assert message maker was specified, an extra param name must also be provided at the end to hold it']]"
        ifFalse: [self error: 'Parameter set size must match the number of names, or 1 less if an assert message maker was specified'].
    paramValidators ifNotNil: [
        1 to: aCollection size do: [:idx | 
            param := aCollection at: idx.
            validator := paramValidators at: idx.
            (((validator isMemberOf: BlockClosure) and: [(validator value: param) = true]) or: [param isKindOf: validator])
                ifFalse: [self error: 'Parameter #', idx asString, ' of set #', (params size + 1) asString, ' failed validation']
        ]
    ].
    assertMessageMaker ifNotNil: [
        (paramNames size ~= (aCollection size + 1))
            ifTrue: [self error: 'Parameter set number must match 1 less the number of names'].
        message := assertMessageMaker value: params size + 1 value: aCollection.
        coll := aCollection asOrderedCollection add: message; yourself.
        ^ params add: coll asArray.
    ].
    ^ params add: aCollection.
]

{ #category : #accessing }
GeaParameterSets >> at: index [
    ^ params at: index
]

{ #category : #accessing }
GeaParameterSets >> initialize [
    params := OrderedCollection new.
    currentParamPointer := 1.
]

{ #category : #accessing }
GeaParameterSets >> makeAssertMessageWith: aBlock [
    "Create a message that will be shown if an assertion fails.
    
    aBlock must take 2 arguments: the index and collection of the parameter set.
    "
    aBlock numArgs = 2 ifFalse: [self error: 'Block for creating the assert message must take 2 arguments: the index and the collection of the parameter set'].
    assertMessageMaker := aBlock.
]

{ #category : #accessing }
GeaParameterSets >> paramNames: aCollection [
    ""
    paramNames := aCollection asArray.
]

{ #category : #accessing }
GeaParameterSets >> paramValidators: aCollectionOfClassesOrBlocks [
    "Set validators."
    (paramNames size = aCollectionOfClassesOrBlocks size or: [paramNames size = (aCollectionOfClassesOrBlocks size + 1)])
        ifFalse: [self error: 'Parameter validator number must match the number of names'].
    aCollectionOfClassesOrBlocks do: [:validator | 
        ((validator class isMemberOf: Metaclass) or: 
            [(validator isMemberOf: BlockClosure) and: [validator numArgs = 1]]
        ) 
            ifFalse: [self error: 'Parameter validators must be classes and/or blocks taking 1 argument']].
    paramValidators := aCollectionOfClassesOrBlocks.
]

{ #category : #accessing }
GeaParameterSets >> size [
    ^ params size.
]
