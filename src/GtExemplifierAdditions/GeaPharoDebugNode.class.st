Class {
	#name : #GeaPharoDebugNode,
	#superclass : #GeaAbstractDebugNode,
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaPharoDebugNode >> isValid [
    ^ [self location class = CompiledMethod] on: Error do: [false].
]

{ #category : #accessing }
GeaPharoDebugNode >> location [
    ^ item methodNode compiledMethod.
]

{ #category : #accessing }
GeaPharoDebugNode >> nodeSrc [
    ^ item formattedCode.
]

{ #category : #accessing }
GeaPharoDebugNode >> printLocation [
    ^ self location methodClass asString, '>>#', self location selector asString.
]

{ #category : #accessing }
GeaPharoDebugNode >> source [
    ^ item source.
]

{ #category : #accessing }
GeaPharoDebugNode >> start [
    ^ item start.
]

{ #category : #accessing }
GeaPharoDebugNode >> stop [
    ^ item stop.
]
