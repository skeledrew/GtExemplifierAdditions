"
I am a wrapper around Process, used to enforce a simplified interface for debugging.
"
Class {
	#name : #GeaPharoDebugProcess,
	#superclass : #GeaAbstractDebugProcess,
	#instVars : [
		'process'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaPharoDebugProcess class >> forContext: aContext priority: anInteger [
    ""
	| newProcess |
	newProcess := Process new.
	newProcess suspendedContext: aContext.
	newProcess priority: anInteger.
	Processor activeProcess installEnvIntoForked: newProcess.
	^ self new process: newProcess.
]

{ #category : #accessing }
GeaPharoDebugProcess >> doesNotUnderstand: aMessage [
    ^ process perform: aMessage selector withArguments: aMessage arguments.
]

{ #category : #accessing }
GeaPharoDebugProcess >> evalable [
    ^ GeaPharoDebugEvalable with: self suspendedContext_method.
]

{ #category : #accessing }
GeaPharoDebugProcess >> newDebugSessionNamed: aString startedAt: aContext [
    sessionName := aString.
    context := aContext.
    ^ session := GeaPharoDebugSession named: aString on: self startedAt: aContext
]

{ #category : #accessing }
GeaPharoDebugProcess >> process [
    ^ process.
]

{ #category : #accessing }
GeaPharoDebugProcess >> process: aProcess [
    process := aProcess.
    ^ self.
]

{ #category : #accessing }
GeaPharoDebugProcess >> suspendedContext_method [
    ^ process suspendedContext method.
]
