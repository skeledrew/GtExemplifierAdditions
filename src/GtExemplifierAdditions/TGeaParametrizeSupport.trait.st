"
I am a trait that enables example parametrization in a class. 

Public API:
- My main method is `#getParamsFor:from:`, which takes a symbol representing an instance variable to hold parameter sets, and a `GeaParameterSets` or a kind of `OrderedCollection` which is the sets of parameters. I highly recommend using the provided `GeaParameterSets`. This method is primarily used within the example itself, or outside in the instance variable where its result must be returned.
- I also provide the convenience method `#runAllExamples` which... runs the examples in my using  class and returns a `GeaParametrizedExampleResults` object containing the aggregated GtExampleResult objects. This is invoked as `MyGeaExampleClass runAllExamples`*.

For a demo, see `GeaParametrizedExampleExample` under the `Examples` tag. Run it in a Playground: `GeaParametrizedExampleExample runAllExamples`.

* To run a single example, do `(MyGeaExampleClass >> #myExampleMethodWith:and:) geaExample run`. It is backwards compatible with `(MyGtExampleClass >> #myExampleMethod) gtExample run`.
"
Trait {
	#name : #TGeaParametrizeSupport,
	#instVars : [
		'isCollectingParametersFlag'
	],
	#category : #'GtExemplifierAdditions-Parametrizing-Core'
}

{ #category : #accessing }
TGeaParametrizeSupport classSide >> runAllExamples [
    "Run each example and return the aggregated results."
    | allExamples results |
	allExamples := GeaParametrizedExampleResults new.
	self methodDict do: [:method | 
	    (method pragmas select: [:p | p selector = #gtExample]) ifNotEmpty: [
	        results := method geaExample run.
	        (method pragmas select: [:p | p selector = #parametrizeUsing:])
	            ifEmpty: [allExamples add: results]
	            ifNotEmpty: [allExamples addAll: results]
	    ]
	].
	^ allExamples.
]

{ #category : #accessing }
TGeaParametrizeSupport classSide >> runExamplesIn: aMethodSelector [
    "Run examples in the selected method."
    | selectedExamples results |
	selectedExamples := GeaParametrizedExampleResults new.
	(self localMethods select: [:m | m selector = aMethodSelector]) do: [:method | 
	    (method pragmas select: [:p | p selector = #gtExample]) ifNotEmpty: [
	        results := method geaExample run.
	        (method pragmas select: [:p | p selector = #parametrizeUsing:])
	            ifEmpty: [selectedExamples add: results]
	            ifNotEmpty: [selectedExamples addAll: results]
	    ]
	].
	^ selectedExamples.
    
]

{ #category : #accessing }
TGeaParametrizeSupport >> getParamsFor: aSymbol from: aBlock [
    "Get parameters for the example."
    | params setter addedSetter setterCode |
    isCollectingParametersFlag ifNil: [^ false].
    [self instVarNamed: aSymbol] 
        on: InstanceVariableNotFound
        do: [self error: 'Instance variable `', aSymbol asString, '` was not found. It is required to store the parameter sets for the current example'].
    setter := (aSymbol asString, ':') asSymbol.
    setterCode := setter asString, ' anObject ', aSymbol asString, ' := anObject'.
    ((self respondsTo: setter) and: [((self class >> setter) sourceCode ~= setterCode)])
        ifTrue: [self error: 'Setter `#', setter, '` exists. To avoid accidentally nuking intended functionality, the setter must not already exist']
        ifFalse: [
        self class compile: setterCode.
        addedSetter := true.
    ].
	[
	    self perform: setter withArguments: {aBlock value}. 
	    GeaParameterCollectionCompleteSignal new signal
	]
        on: GeaParameterCollectionCompleteSignal 
        do: [
            addedSetter ifTrue: [self class removeSelector: setter].
            ^ GeaParameterCollectionCompleteSignal new signal. "reraise to break out, if collecting within an example"
        ].
]

{ #category : #accessing }
TGeaParametrizeSupport >> isCollectingParametersFlag [
    "redundant?"
    Halt now.
    ^ isCollectingParametersFlag.
]

{ #category : #accessing }
TGeaParametrizeSupport >> isCollectingParametersFlag: aBoolean [
    isCollectingParametersFlag := aBoolean.
]
