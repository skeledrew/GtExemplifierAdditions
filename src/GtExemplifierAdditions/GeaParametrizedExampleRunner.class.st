"
I'm pretty obsolete, but maybe I'll stick around for a while anyway...
"
Class {
	#name : #GeaParametrizedExampleRunner,
	#superclass : #Object,
	#classVars : [
		'currentPartialId',
		'paramsData'
	],
	#category : #'GtExemplifierAdditions-_Obsolete'
}

{ #category : #accessing }
GeaParametrizedExampleRunner class >> allParamsData [
    ^ paramsData
]

{ #category : #accessing }
GeaParametrizedExampleRunner class >> allParamsData: anObject [
    paramsData := anObject
]

{ #category : #accessing }
GeaParametrizedExampleRunner class >> currentPartialId [
    ^ currentPartialId.
]

{ #category : #accessing }
GeaParametrizedExampleRunner >> initialize [
    self class allParamsData ifNil: [self class allParamsData: Dictionary new].
]

{ #category : #accessing }
GeaParametrizedExampleRunner >> runWith: aCompiledMethod [
    "Run the example with the params within it."
    | results pragmas paramsVariable dummyArgs selector aParameterizedExampleObject cls params runId ex secs |
	results := GeaParametrizedExampleResults new.
	pragmas := aCompiledMethod pragmas.
	paramsVariable := (pragmas select: [:p | p selector = #parametrizeUsing:]) first arguments first.
	dummyArgs := OrderedCollection new.
	selector := aCompiledMethod selector.
	1 to: (selector numArgs) do: [:idx | dummyArgs add: nil].
	cls := aCompiledMethod methodClass.
	aParameterizedExampleObject := cls new.
	aParameterizedExampleObject isCollectingParametersFlag: true.
	[aParameterizedExampleObject perform: selector withArguments: dummyArgs asArray] on: GeaParameterCollectionCompleteSignal do: [nil].
	aParameterizedExampleObject isCollectingParametersFlag: false.
	params := aParameterizedExampleObject perform: paramsVariable.
	secs := DateAndTime new asSeconds.
	runId := secs asString, '::', cls name, '::', selector asString.
	"self class allParamsData at: runId put: params."
	currentPartialId := secs.
	1 to: params size do: [:idx | 
	    self class allParamsData at: runId put: (params at: idx).
	    ex := (cls >> #run) gtExample.
	    ex ifNil: [self error: 'Looks like the runId got messed up'].
	    results add: ex
	].
	^ results
]
