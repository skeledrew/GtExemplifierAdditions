"
I hold multiple related trace results, and potentially provide consolidated views for them.
"
Class {
	#name : #GeaTraceResultsGroup,
	#superclass : #OrderedCollection,
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaTraceResultsGroup >> printOn: aStream [
    super printOn: aStream.
    aStream
        nextPutAll: self class name;
        nextPutAll: ' [';
        nextPutAll: self size asString;
        nextPutAll: ' result';
        nextPutAll: (self size = 1 ifTrue:[''] ifFalse: ['s']);
        nextPut: $].
]
