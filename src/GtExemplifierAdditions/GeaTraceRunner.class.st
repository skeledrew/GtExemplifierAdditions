"
I facilitate the trace-debugging of methods.
"
Class {
	#name : #GeaTraceRunner,
	#superclass : #Object,
	#instVars : [
		'session',
		'process',
		'context',
		'nodes',
		'varsColl',
		'methods',
		'traceResult',
		'snapTimes',
		'editor',
		'recordLevel',
		'currentLevel',
		'resultsGroup',
		'startTime',
		'targetMethod',
		'previousNode',
		'previousLevel',
		'loopCount',
		'loopsColl',
		'levelsColl',
		'methodStack',
		'stepIntoFilterBlock',
		'pointLogger',
		'traceProc',
		'processClass',
		'pauseRun',
		'stopBlock',
		'startPoint'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaTraceRunner class >> traceMethod: aCompiledMethod from: aBlock toDepth: aDepthInteger withFilter: aFilterBlock [
    ^ self new
        traceMethod: aCompiledMethod
        from: aBlock
        toDepth: aDepthInteger
        withFilter: aFilterBlock.
]

{ #category : #accessing }
GeaTraceRunner >> collectVars [
    ^ self session contextVariables.
]

{ #category : #accessing }
GeaTraceRunner >> context [
    ^ context.
]

{ #category : #accessing }
GeaTraceRunner >> defaultStopBlockInitBlock [
    ^ [:startPointArg |
        | stopNode |
        stopNode := startPointArg node innerNode.
        (stopNode isKindOf: RBProgramNode) ifFalse: [self error: 'Expected a Pharo program node, not ', stopNode class name].
        [stopNode class = RBBlockNode] whileFalse: [stopNode := stopNode parent].
        [:point | point node innerNode = stopNode]
    ].
]

{ #category : #accessing }
GeaTraceRunner >> initCollectors [
    ""
    editor := nil.
    recordLevel := currentLevel.
    nodes := OrderedCollection new.
    snapTimes := OrderedCollection new.
    traceResult := GeaTraceResult new.
    resultsGroup add: traceResult.
    methods := OrderedCollection new.
    varsColl := OrderedCollection new.
    loopsColl := OrderedCollection new.
    levelsColl := OrderedCollection new.
]

{ #category : #accessing }
GeaTraceRunner >> initStopBlockFrom: aGeaTracePoint [
    "Find default stop block node for Pharo code."
    | stopNode |
	stopBlock := self defaultStopBlockInitBlock value: aGeaTracePoint.
]

{ #category : #accessing }
GeaTraceRunner >> initialize [
    currentLevel := 0.
    recordLevel := 999.
    loopCount := 0.
    resultsGroup := GeaTraceResultsGroup new.
    methodStack := LinkedList new.  "TODO: Disable when not debugging"
    stepIntoFilterBlock := [:level :method :node :point | false].
    processClass := GeaPharoDebugProcess.
]

{ #category : #accessing }
GeaTraceRunner >> makeEditorFor: aNode [
    ^ BrEditor new
        aptitude: BrGlamorousCodeEditorAptitude;
        styler:
            (BrRBTextStyler new
                classOrMetaClass: aNode methodNode compiledMethod methodClass);
        text: aNode source.
]

{ #category : #accessing }
GeaTraceRunner >> pauseRun [
    ^ pauseRun ifNil: [pauseRun := false].
]

{ #category : #accessing }
GeaTraceRunner >> pauseRun: aBoolean [
    pauseRun := aBoolean.
]

{ #category : #accessing }
GeaTraceRunner >> process [
    ^ process.
]

{ #category : #accessing }
GeaTraceRunner >> recordResults [
    ""
    recordLevel := 999.
    1 to: snapTimes size do: [:idx |
        | point |
        point := GeaTracePoint new
            startTime: startTime;
            snapTime: (snapTimes at: idx);
            node: (nodes at: idx);
            editor: editor;
            method: (methods at: idx);
            variables: (varsColl at: idx);
            level: (levelsColl at: idx);
            loop: (loopsColl at: idx).
        traceResult add: point.
    ].
    "resultsGroup add: traceResult."
]

{ #category : #accessing }
GeaTraceRunner >> recordSnapPointFor: aNode in: aGeaAbstractDebugEvalable [
    "Write points to the results object while tracing.
    
    Used to make available live results for long running traces.
    "
    | point |
	point := GeaTracePoint new
        startTime: startTime;
        snapTime: DateAndTime now;
        node: aNode;
        "editor: editor;"
        evalable: aGeaAbstractDebugEvalable;
        variables: self collectVars;
        level: currentLevel;
        loop: loopCount.
    GeaTracePointSignal emit: point.
    point isValid ifTrue: [traceResult add: point].
    ^ point.
]

{ #category : #accessing }
GeaTraceRunner >> run [
    "Start a preconfigured trace.
    
    If pointLogger is set then the trace is automatically ran in a separate process and the logger is returned. Otherwise the results are returned after the trace completes.
    "
    | runBlock |
    startPoint ifNil: [self error: 'A start block is required'].
	runBlock := [self run: startPoint].
	pointLogger ifNotNil: [
	    traceProc := [pointLogger
	        runFor: GeaTracePointSignal
	        during: [runBlock value].
	    ] forkAt: 29 named: 'trace'.
	    ^ pointLogger.
	].
	^ runBlock value.
]

{ #category : #accessing }
GeaTraceRunner >> run: aBlock [
    ""
    | currentEvalable currentNode previousEvalable currentPoint |
	self setUpSessionAndProcessAndContextFor: aBlock.
	self initCollectors.
	"SkelSingleRegistry at: #currentOrLastRunTraceResult put: resultsGroup.
	SkelSingleRegistry at: #currentOrLastRunTraceRunner put: self."
	currentNode := false.
	currentPoint := GeaTracePoint new.
	currentPoint node: false.
	startTime := DateAndTime now.
    [self terminateOn: currentPoint] whileFalse: [
	    "Terminate when we get to the end of the block's node."
	    self pauseRun ifTrue: [Halt now].
	    (currentEvalable isNil or: [(stepIntoFilterBlock value: currentLevel value: currentEvalable value: currentNode value: currentPoint variables) not])
            ifTrue: [self session stepInto]
            ifFalse: [self session stepOver].
	    currentEvalable := self process evalable.
        currentNode := [
            currentEvalable sourceNode: self session
        ] on: Error do: [:ex | Halt now. ex debug].
        (currentEvalable ~= previousEvalable) ifTrue: [
            self updateOnChangeTo: currentEvalable at: currentNode.
        ].
        "lastPoint := self snapPointInfoFor: currentNode in: currentMethod."
        currentPoint := self recordSnapPointFor: currentNode in: currentEvalable.
        stopBlock ifNil: [self initStopBlockFrom: currentPoint].
        previousEvalable := currentEvalable.
        previousNode := currentNode.
        loopCount := loopCount + 1.
	].
	traceResult ifEmpty: [self recordResults].
	^ resultsGroup.
]

{ #category : #accessing }
GeaTraceRunner >> session [
    ^ session.
]

{ #category : #accessing }
GeaTraceRunner >> setPointLoggerTo: aLogger [
    "A logger to access intermediate results for especially long traces."
    (aLogger isKindOf: MemoryLogger) ifFalse: [self error: 'Only MemoryLogger (and it''s subclass) objects are valid'].
    pointLogger := aLogger.
]

{ #category : #accessing }
GeaTraceRunner >> setPreventStepIntoFilterTo: aBlock [
    "Set aBlock which, if true, causes a StepOver action instead of StepInto.
    
    Other actions can also be done in the block.
    
    aBlock uses the currentLevel, currentMethod, currentNode and currentPoint as arguments.
    "
    aBlock numArgs = 4 ifFalse: [self error: 'aBlock takes 4 arguments: level, method, node and point'].
    stepIntoFilterBlock := aBlock.
]

{ #category : #accessing }
GeaTraceRunner >> setProcessClass: aClass [
    "Set the debugging process class.
    
    This is used as a flag to determine the language being traced.
    "
    processClass := aClass.
]

{ #category : #accessing }
GeaTraceRunner >> setStartBlockTo: aBlock [
    "Point from which to start running a trace."
    (aBlock class = BlockClosure & (aBlock numArgs = 0))
        ifFalse: [self error: 'Must be a BlockClosure taking 0 args'].
    startPoint := aBlock.
]

{ #category : #accessing }
GeaTraceRunner >> setStartPointTo: anObject [
    "Point from which to start running a trace."
    startPoint := anObject.
]

{ #category : #accessing }
GeaTraceRunner >> setStopBlockTo: aBlock [
    stopBlock := aBlock.
]

{ #category : #accessing }
GeaTraceRunner >> setUpSessionAndProcessAndContextFor: aBlock [
	context := aBlock asContext.
	process := processClass 
	    forContext: context 
	    priority: Processor userInterruptPriority.
	session:= process newDebugSessionNamed: 'tracing session' startedAt: context.
]

{ #category : #accessing }
GeaTraceRunner >> snapPointInfoFor: aNode in: aCompiledMthod [
    snapTimes add: DateAndTime now.
            editor ifNil: [
                editor := self makeEditorFor: aNode
            ].  "NOTE: We need an editor for each recorded method"
            nodes add: aNode.
            methods add: aCompiledMthod.
            varsColl add: self collectVars.
            loopsColl add: loopCount.
            levelsColl add: currentLevel.
]

{ #category : #accessing }
GeaTraceRunner >> terminateOn: currentPoint [
    stopBlock ifNil: [^ false].
    ^ stopBlock value: currentPoint.
]

{ #category : #accessing }
GeaTraceRunner >> traceMethod: aCompiledMethod from: aBlock toDepth: aDepthInteger withFilter: aFilterBlock [
    "Record the nodes in a method's context.
    
    Step through aBlock and record only when within or below aCompiledMethod down to aDethInteger (relative to the method). Use aFilterBlock to determine if the current method's nodes should be recorded.
    "
    | startMethod leftStart node currentMethod previousMethod resultColl |
	self setUpSessionAndProcessAndContextFor: aBlock.
	targetMethod := aCompiledMethod.
    startMethod := currentMethod := previousMethod := self process suspendedContext method.
    leftStart := false.
    startTime := DateAndTime now.
    [currentMethod = startMethod and: [leftStart and: [node class = RBBlockNode]]] whileFalse: [
        "Terminate loop when we're back in the start method and at the block node"
        (loopCount > 0) & (loopCount % 1000 = 0) ifTrue: [Halt now].
        ((currentLevel - recordLevel) >= aDepthInteger or: [{ProtoObject. Context} anySatisfy: [:cls | cls = currentMethod class]])
            ifTrue: [self session stepOver]
            ifFalse: [self session stepInto].
        currentMethod := self process suspendedContext method.
        node := [currentMethod sourceNodeForPC: self session interruptedContext pc]
            on: Error do: [:ex | Halt now. ex].
        currentMethod ~= previousMethod ifTrue: [
            self updateOnChangeTo: currentMethod.
        ].
        (((currentLevel >= recordLevel) & (currentLevel <= (recordLevel + aDepthInteger)) or: [currentMethod = aCompiledMethod]) and: [aFilterBlock value: currentMethod])
            ifTrue: [ self snapPointInfoFor: node in: currentMethod].
        ((leftStart = false) and: [currentMethod ~= startMethod])
            ifTrue: [leftStart := true].
        previousMethod := currentMethod.
        previousLevel := currentLevel.
        previousNode := node.
        loopCount := loopCount + 1.
    ].
    recordLevel ~= 999 ifTrue: [self recordResults].
    ^ resultsGroup.
]

{ #category : #accessing }
GeaTraceRunner >> updateOnChangeTo: aCompiledMethod [
    ""
    previousNode class = RBMessageNode ifTrue: [
        currentLevel := currentLevel + 1.
        methodStack addFirst: aCompiledMethod.
        (aCompiledMethod = targetMethod) & (recordLevel = 999) ifTrue: [
            "Entered area of interest."
            self initCollectors.
        ].
    ]".
    (previousNode class = RBReturnNode or: [node source size = node stop]) ifTrue:" ifFalse: [
        currentLevel := currentLevel - 1.
        methodStack removeFirst.
        (currentLevel < recordLevel and: [recordLevel ~= 999]) ifTrue: [
            "Exited area of interest"
            self recordResults.
        ].
    ].
    "(previousLevel = currentLevel and: [recordLevel ~= 999])
        ifTrue: [self error: 'Current method changed but level did not']."
]

{ #category : #accessing }
GeaTraceRunner >> updateOnChangeTo: currentMethod at: currentNode [
    previousNode class = RBMessageNode
        ifTrue: [currentLevel := currentLevel + 1]
        ifFalse: [currentLevel := currentLevel - 1].
]
