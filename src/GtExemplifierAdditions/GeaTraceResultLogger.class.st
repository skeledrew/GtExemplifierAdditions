Class {
	#name : #GeaTraceResultLogger,
	#superclass : #MemoryLogger,
	#category : #'GtExemplifierAdditions-Debugging-Wrappers'
}

{ #category : #accessing }
GeaTraceResultLogger >> gtResultsFor: aView [
	<gtView>
	^ aView columnedList
		title: 'Results' translated;
		priority: 40;
		items: [ self recordings reverse ];
		updateWhen: Announcement in: [ self announcer ];
		column: 'Timestamp' translated item: [ :each | each timestamp asString ];
		"column: 'Process' translated item: [ :each | each processId asString ];
		column: 'Type' translated item: [ :each | each name ];"
		column: 'Contents' translated 
			item: [ :each | String streamContents: [ :s | each printOneLineContentsOn: s ] ]
]

{ #category : #accessing }
GeaTraceResultLogger >> gtTimeTraceFor: aView [
    <gtView>
    ^ recordings gtTimeTraceFor: aView.
]

{ #category : #accessing }
GeaTraceResultLogger >> initialize [
    recordings := GeaTraceResult new.
    name := 'n/a'.
]
