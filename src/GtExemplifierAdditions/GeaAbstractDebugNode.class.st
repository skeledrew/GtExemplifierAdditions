Class {
	#name : #GeaAbstractDebugNode,
	#superclass : #Object,
	#instVars : [
		'item'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaAbstractDebugNode class >> from: anObject [
    ^ self new
        from: anObject;
        yourself.
]

{ #category : #accessing }
GeaAbstractDebugNode >> doesNotUnderstand: aMessage [
    ^ item perform: aMessage selector withArguments: aMessage arguments.
]

{ #category : #accessing }
GeaAbstractDebugNode >> from: anObject [
    item := anObject.
]

{ #category : #accessing }
GeaAbstractDebugNode >> innerNode [
    ^ self node.
]

{ #category : #accessing }
GeaAbstractDebugNode >> isValid [
    "Return true if the node is valid.
    
    Primarily used to check if a GeaTracePoint should be included in the results.
    "
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugNode >> location [
    "Return an object representing where the node is located."
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugNode >> node [
    ^ item.
]

{ #category : #accessing }
GeaAbstractDebugNode >> nodeSrc [
    "Return the node's source code.
    
    NOTE: This is the effective code only, not the surrounding.
    "
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugNode >> printLocation [
    "Return the string representation of the node's location."
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugNode >> source [
    "Return the node (and surrounding) source code."
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugNode >> start [
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugNode >> stop [
    self subclassResponsibility.
]
