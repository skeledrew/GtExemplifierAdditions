"
I am a subclass of OrderedCollection,  intended for the collection of GtExampleResult objects.

Public API:
- I am returned by `CompiledMethod >> #geaExample` and `TGeaParametrizeSupport >> #runAllExamples`.
- I provide the `#returnValuesAndExampleExceptions` method, which is a simple proxy to `#returnValueOrExampleException`.
- I automatically proxy any messages I do not understand to all my contained GtExampleResult objects.
"
Class {
	#name : #GeaParametrizedExampleResults,
	#superclass : #OrderedCollection,
	#category : #'GtExemplifierAdditions-Parametrizing-Core'
}

{ #category : #accessing }
GeaParametrizedExampleResults >> doesNotUnderstand: aMessage [
    "Pass everything through to the individual GtExampleResult objects."
    ^ self collect: 
        [:ger | ger perform: aMessage selector withArguments: aMessage arguments].
]

{ #category : #accessing }
GeaParametrizedExampleResults >> gtResultsFor: aView [
    <gtView>
    self size isZero ifTrue: [^ aView empty].
    ^ aView columnedList
        title: (self size = 1 ifTrue: ['Result'] ifFalse: ['Results']);
        priority: 5;
        items: self;
        column: 'Result' text: [:res |
            res hasException not
                ifTrue: [res returnValue gtDisplayString asRopedText]
                ifFalse: [res exampleException description asRopedText foreground: Color red]
        ];
        send: [:res | res hasException ifTrue: [res exampleException] ifFalse: [res returnValue]].
]

{ #category : #accessing }
GeaParametrizedExampleResults >> returnValuesAndExampleExceptions [
    "Convenience proxy."
    ^ self collect: [:ger | ger returnValueOrExampleException]
]
