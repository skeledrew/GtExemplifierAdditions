"
I am a simple way to abort the run of an example during the parameter collection phase, if the collection is done within the example.
"
Class {
	#name : #GeaParameterCollectionCompleteSignal,
	#superclass : #Error,
	#category : #'GtExemplifierAdditions-Parametrizing-Core'
}
