Class {
	#name : #GeaAbstractDebugContext,
	#superclass : #Object,
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaAbstractDebugContext >> asContext [
    ^ self
]
