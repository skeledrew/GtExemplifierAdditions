Extension { #name : #CompiledMethod }

{ #category : #'*GtExemplifierAdditions' }
CompiledMethod >> geaExample [
    "Return a GtExample or GeaExample."
    | ex numArgsSrc |
    (self pragmas select: [:p | p selector = #parametrizeUsing:])
        ifNotEmpty: [
	        ex := GeaParametrizedExample new example: self gtParametrizedExample.
	        ex initializeFromMethod: self withProviderClass: self methodClass usingFactory: GtParametrizedExampleFactory new.
	        ^ ex
	    ].
	^ self gtExample.
]
