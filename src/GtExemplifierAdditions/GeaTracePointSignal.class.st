"
I am a Beacon signal specifically for sending GeaTracePoint objects to a running logger.
"
Class {
	#name : #GeaTracePointSignal,
	#superclass : #WrapperSignal,
	#category : #'GtExemplifierAdditions-Debugging-Wrappers'
}

{ #category : #accessing }
GeaTracePointSignal >> gtDetailsFor: aView [
    <gtView>
    ^ target gtDetailsFor: aView.
]

{ #category : #accessing }
GeaTracePointSignal >> gtOneLineFor: aView [
    <gtView>
    ^ aView empty.
]

{ #category : #accessing }
GeaTracePointSignal >> gtOverviewFor: aView [
    <gtView>
    ^ target gtOverviewFor: aView.
]

{ #category : #accessing }
GeaTracePointSignal >> gtReferencesFor: aView [
    <gtView>
    ^ (super gtReferencesFor: aView)
        priority: 20.
]

{ #category : #accessing }
GeaTracePointSignal >> gtSourceFor: aView [
    <gtView>
    ^ target gtSourceFor: aView.
]

{ #category : #accessing }
GeaTracePointSignal >> gtVariablesFor: aView [
    <gtView>
    ^ target gtVariablesFor: aView.
]

{ #category : #accessing }
GeaTracePointSignal >> target: aGeaTracePoint [
    ({GeaTracePoint} anySatisfy: [:cls | cls = aGeaTracePoint class])
        ifFalse: [self error: 'This signal is for sending GeaTracePoint objects'].
    target := aGeaTracePoint.
]
