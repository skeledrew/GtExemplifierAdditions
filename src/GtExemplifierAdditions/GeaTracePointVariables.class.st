"
I am a collection for variable-value pairs that allows dictionary access.

TODO: Convert to use OrderPreservingDictionary
"
Class {
	#name : #GeaTracePointVariables,
	#superclass : #OrderedCollection,
	#instVars : [
		'dict'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaTracePointVariables class >> from: aDictionary [
    | vars |
	vars := self new.
    aDictionary keysAndValuesDo: [:key :value | vars add: {key . value}].
    ^ vars.
]

{ #category : #accessing }
GeaTracePointVariables class >> new [
    ^ super new initialize.
]

{ #category : #accessing }
GeaTracePointVariables >> get: aSymbol [
    ^ [dict at: aSymbol ifAbsentPut: (self select: [:pair | pair first = aSymbol]) first second]
        on: SubscriptOutOfBounds
        do: [:ex | {aSymbol. GeaTracePointException new exception: ex}].
]

{ #category : #accessing }
GeaTracePointVariables >> initialize [
    super initialize.
    dict := Dictionary new.
]
