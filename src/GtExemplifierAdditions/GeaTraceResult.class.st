"
I hold a collection of trace points which together form the results of a trace session.
"
Class {
	#name : #GeaTraceResult,
	#superclass : #OrderedCollection,
	#instVars : [
		'session',
		'process',
		'context',
		'nodes',
		'editor'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaTraceResult >> context [
    ^ context.
]

{ #category : #accessing }
GeaTraceResult >> gtTimeTraceFor: aView [
    <gtView>
    | nodeViews |
    self ifEmpty: [^ aView empty].
    ^ aView columnedList
        title: 'Time Trace';
        priority: 5;
        items: self;
        "column: 'Item' text: [:node | editor dataSource text gtDisplayText copyFrom: node start to: node stop];"
        column: 'Index' 
			item: [ :eachItem :eachIndex | 
				eachIndex asRopedText foreground: Color gray ]
			width: 45;
		column: 'Inc/Lvl' text: [:point |
		    point loop asString, '-', point level asString
		] width: 60;
        column: 'Time offset' text: [:point | (point times second - point times first) asString] width: 110;
        column: 'Code' text: [:point | point node nodeSrc gtDisplayText asRopedText];
        column: 'Location' text: [:point |
            point node printLocation
        ];
        actionUpdateButton.
]

{ #category : #accessing }
GeaTraceResult >> nodes [
    ^ nodes
]

{ #category : #accessing }
GeaTraceResult >> process [
    ^ process.
]

{ #category : #accessing }
GeaTraceResult >> session [
    ^ session
]
