Class {
	#name : #GeaAbstractDebugSession,
	#superclass : #Object,
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaAbstractDebugSession >> contextVariables [
    self subclassResponsibility
]

{ #category : #accessing }
GeaAbstractDebugSession >> stepInto [
    self subclassResponsibility
]

{ #category : #accessing }
GeaAbstractDebugSession >> stepOver [
    self subclassResponsibility
]
