"
I help pass and signal exceptions in contexts where exception objects may be legitimate, such as variable values.
"
Class {
	#name : #GeaTracePointException,
	#superclass : #Object,
	#instVars : [
		'exc'
	],
	#category : #'GtExemplifierAdditions-Debugging-Wrappers'
}

{ #category : #accessing }
GeaTracePointException >> exception [
    ^ exc.
]

{ #category : #accessing }
GeaTracePointException >> exception: anException [
    exc := anException.
]
