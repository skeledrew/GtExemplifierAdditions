Class {
	#name : #GeaTraceRunnerExample,
	#superclass : #Object,
	#traits : 'TGeaParametrizeSupport',
	#classTraits : 'TGeaParametrizeSupport classTrait',
	#instVars : [
		'traceMethodParams'
	],
	#category : #'GtExemplifierAdditions-Debugging-Examples'
}

{ #category : #accessing }
GeaTraceRunnerExample >> another: aNumber [
    "Maybe docstring."
    | var1 check |
	var1 := 5.
    var1 > 3 ifTrue: [
        var1 := 1.
        check := true.
        ^ self answer.
    ].
    ^ check.
]

{ #category : #accessing }
GeaTraceRunnerExample >> answer [
    ^ 40 + 2.
]

{ #category : #accessing }
GeaTraceRunnerExample >> causeError [
    1 / 0.
]

{ #category : #accessing }
GeaTraceRunnerExample >> handleBlock [
    0.
    [1] on: Error do: [2].
    3.
]

{ #category : #accessing }
GeaTraceRunnerExample >> runFrom: aStartBlock preventStepIntoWhen: aPreventStepBlock andAssertWith: anAssertBlock [
    ""
    <gtExample>
    <parametrizeUsing: #traceMethodParams>
    | result methodStr |
	self getParamsFor: #traceMethodParams from: [
        GeaParameterSets new
            paramNames: #(aStartBlock aPreventStepBlock anAssertBlock);
            + {
                [self another: 3].
                [:lvl :ev :n :pnt | lvl >= 3].
                [:res | res size = 1 and: [res first size = 10]].
            };
            + {
                [self causeError].
                [:lvl :ev :n :pnt | lvl >= 1].
                [:res | res size = 1 and: [res first size = 7]].
            };
            + {
                [self handleBlock].
                [:lvl :ev :n :pnt | lvl >= 3].
                [:res | res size = 1 and: [res first size = 7]].
            };
            + {
                [self causeError].
                [:lvl :ev :n :pnt | lvl >= 4].
                [:res | res size = 1 and: [res first size = 48]].
            }.
    ].
    result := GeaTraceRunner new
        setPreventStepIntoFilterTo: aPreventStepBlock;
        setStartBlockTo: aStartBlock;
        run.
    self assert: (anAssertBlock value: result) description: 'Failed on: ', aStartBlock asString.
    ^ result.
]

{ #category : #accessing }
GeaTraceRunnerExample >> traceMethod: aCompiledMethod from: aStartBlock toDepth: aDepthInteger andAssertWith: anAssertBlock [
    ""
    <gtExample>
    <parametrizeUsing: #traceMethodParams>
    | result methodStr |
	self getParamsFor: #traceMethodParams from: [
        GeaParameterSets new
            paramNames: #(aCompiledMethod aStartBlock aDepthInteger anAssertBlock);
            + {
                GeaTraceRunnerExample>>#another:.
                [self another: 3].
                3.
                [:res | res size = 1 and: [res first size = 8]].
            };
            + {
                GeaTraceRunnerExample>>#causeError.
                [self causeError].
                1.
                [:res | res size = 1 and: [res first size = 7]].
            };
            + {
                GeaTraceRunnerExample>>#handleBlock.
                [self handleBlock].
                3.
                [:res | res size = 1 and: [res first size = 7]].
            };
            + {
                GeaTraceRunnerExample>>#causeError.
                [self causeError].
                4.
                [:res | res size = 1 and: [res first size = 48]].
            }.
    ].
    result := GeaTraceRunner traceMethod: aCompiledMethod from: aStartBlock toDepth: aDepthInteger withFilter: [:method | true].
    methodStr := aCompiledMethod methodClass asString, '>>#', aCompiledMethod selector asString.
    self assert: (anAssertBlock value: result) description: 'Failed on `', methodStr, '` at depth: ', aDepthInteger asString.
    ^ result.
]
