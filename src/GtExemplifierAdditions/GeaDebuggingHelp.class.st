"
#Debugging

##Overview
There are - currently - 2 debugging-related components in GEA:
- A tracer, which allows code to be run and a trace of the executed nodes recorded.
- A language-agnostic debugger interface, which allows code in any executable language accessible in Pharo to be debugged.

##Setup
To add support for a language, subclasses of the `GeaAbstractDebug*` classes are required, with appropriate implementations of the key methods.
"
Class {
	#name : #GeaDebuggingHelp,
	#superclass : #Object,
	#category : #'GtExemplifierAdditions-Documentation'
}
