Class {
	#name : #GeaAbstractDebugEvalable,
	#superclass : #Object,
	#instVars : [
		'evalable'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaAbstractDebugEvalable >> doesNotUnderstand: aMessage [
    evalable perform: aMessage selector withArguments: aMessage arguments.
]

{ #category : #accessing }
GeaAbstractDebugEvalable >> evalable [
    ^ evalable
]

{ #category : #accessing }
GeaAbstractDebugEvalable >> evalable: anObject [
    evalable := anObject.
]

{ #category : #accessing }
GeaAbstractDebugEvalable >> sourceNode: aGeaAbstractDebugSession [
    self subclassResponsibility.
]
