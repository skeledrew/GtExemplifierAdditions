Class {
	#name : #GeaPharoDebugEvalable,
	#superclass : #GeaAbstractDebugEvalable,
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaPharoDebugEvalable class >> with: aCompiledMethod [
    ^ self new
        evalable: aCompiledMethod;
        yourself.
]

{ #category : #accessing }
GeaPharoDebugEvalable >> inner [
    ^ evalable.
]

{ #category : #accessing }
GeaPharoDebugEvalable >> methodClass [
    ^ evalable methodClass.
]

{ #category : #accessing }
GeaPharoDebugEvalable >> printOn: aStream [
    super printOn: aStream.
    aStream
        nextPutAll: ' (', evalable methodClass asString, '>>', evalable selector asString, ')'.
]

{ #category : #accessing }
GeaPharoDebugEvalable >> selector [
    ^ evalable selector.
]

{ #category : #accessing }
GeaPharoDebugEvalable >> sourceNode: aGeaAbstractDebugSession [
    ^ GeaPharoDebugNode from:
        (evalable sourceNodeForPC: aGeaAbstractDebugSession interruptedContext_pc).
]
