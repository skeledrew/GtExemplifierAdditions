"
I am the abstract class of a process running code that is being debugged.
"
Class {
	#name : #GeaAbstractDebugProcess,
	#superclass : #Object,
	#instVars : [
		'sessionName',
		'context',
		'session'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaAbstractDebugProcess class >> forContext: aDebugContext priority: anInteger [
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugProcess >> evalable [
    self subclassResponsibility.
]

{ #category : #accessing }
GeaAbstractDebugProcess >> newDebugSessionNamed: aString startedAt: aContext [
    self subclassResponsibility
]
