"
I am a wrapper for `GtExampleEvaluator` and I facilitate processing example parameters.
"
Class {
	#name : #GeaExampleEvaluator,
	#superclass : #GtExampleEvaluator,
	#instVars : [
		'paramsExample'
	],
	#category : #'GtExemplifierAdditions-Parametrizing-Wrappers'
}

{ #category : #accessing }
GeaExampleEvaluator >> result [
    "Evaluate the example with each param set."
    <return: #GeaParametrizedExampleResults>
    | results |
	results := GeaParametrizedExampleResults new.
	paramsExample := self example.
    1 to: paramsExample paramSets size do: [:idx |
        self example: (paramsExample at: idx).
        results add: super result.
    ].
    self example: paramsExample.
    ^ results.
]
