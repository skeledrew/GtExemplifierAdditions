Class {
	#name : #GeaPharoDebugSession,
	#superclass : #GeaAbstractDebugSession,
	#instVars : [
		'session',
		'process',
		'context',
		'sessionName'
	],
	#category : #'GtExemplifierAdditions-Debugging-Core'
}

{ #category : #accessing }
GeaPharoDebugSession class >> named: aString on: aGeaPharoDebugProcess startedAt: aContext [
    ""
    ^ self new
        sessionName: aString;
        process: aGeaPharoDebugProcess;
        context: aContext;
        session: (aGeaPharoDebugProcess process
            newDebugSessionNamed: aString
            startedAt: aContext);
        yourself.
]

{ #category : #accessing }
GeaPharoDebugSession >> context: aContext [
    context := aContext.
]

{ #category : #accessing }
GeaPharoDebugSession >> contextVariables [
    | vars |
	vars := GeaTracePointVariables new.
	(self gtViewLiveDebuggerFor: GtPhlowView empty) asElement contextVariables do: [:var | vars add: {var key . [var rawValue] on: Error do: [:ex | GeaTracePointException new exception: ex]}.
    ].
    ^ vars.
]

{ #category : #accessing }
GeaPharoDebugSession >> doesNotUnderstand: aMessage [
    ^ session perform: aMessage selector withArguments: aMessage arguments.
]

{ #category : #accessing }
GeaPharoDebugSession >> interruptedContext_pc [
    ^ session interruptedContext pc.
]

{ #category : #accessing }
GeaPharoDebugSession >> process: aGeaPharoDebugProcess [
    process := aGeaPharoDebugProcess.
]

{ #category : #accessing }
GeaPharoDebugSession >> session: aDebugSession [
    session := aDebugSession.
]

{ #category : #accessing }
GeaPharoDebugSession >> sessionName: aString [
    sessionName := aString.
]

{ #category : #accessing }
GeaPharoDebugSession >> stepInto [
    ^ session stepInto.
]

{ #category : #accessing }
GeaPharoDebugSession >> stepOver [
    ^ session stepOver.
]
