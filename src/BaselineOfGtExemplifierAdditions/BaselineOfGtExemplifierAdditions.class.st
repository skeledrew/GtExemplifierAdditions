Class {
	#name : #BaselineOfGtExemplifierAdditions,
	#superclass : #BaselineOf,
	#category : #BaselineOfGtExemplifierAdditions
}

{ #category : #accessing }
BaselineOfGtExemplifierAdditions >> baseline: spec [
    <baseline>
    spec for: #common do: [
        spec
            "package: 'GToolkit-Examples';"
            package: 'GtExemplifierAdditions'.
    ].
]
